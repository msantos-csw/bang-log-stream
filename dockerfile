FROM golang:1.15.6 as build-env

WORKDIR /go/src/app
COPY . /go/src/app

RUN go get -d -v ./...

RUN go build -ldflags="-s -w" -o /go/bin/app

RUN setcap cap_net_raw=+ep /go/bin/app

FROM gcr.io/distroless/base
COPY --from=build-env /go/bin/app /

ENTRYPOINT [ "/app" ]