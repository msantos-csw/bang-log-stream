package main

import (
	"log"
	"time"
)

func main() {
	for {
		log.Println("INFO: This is an info message")
		log.Println("ERROR: This is an error message")
		time.Sleep(10 * time.Second)
	}
}